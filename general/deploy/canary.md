## Overview

The canary stage is a subset of fleet within the production and staging
environment that can be deployed to independently of the main environment.

* When querying in prometheus, metrics for canary are labeled as `stage=cny`
* Non-canary boxes are labeled as `stage=main`
* The [dashboard for canary](https://dashboards.gitlab.net/d/llfd4b2ik/canary?orgId=1)
  has the current version as well as some canary specific metrics

The following backends support canary traffic:

* API
* HTTPS GIT
* Registry
* Web

By default, all web requests with the cookie `gitlab_canary=true` are
directed to canary. In addition to this, certain request paths
are sent, see the [handbook page](https://about.gitlab.com/handbook/engineering/#canary-testing)
on canary for more information about what traffic is sent there and how to opt-in and
opt-out.

Request paths for canary are configured in chef for staging and production:

* Production
    * https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/master/roles/gprd-base-lb-registry.json#L8
    * https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/master/roles/gprd-base-lb-fe-common.json#L9
* Staging
    * https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/master/roles/gstg-base-lb-fe-common.json#L9
    * https://ops.gitlab.net/gitlab-cookbooks/chef-repo/blob/master/roles/gstg-base-lb-registry.json#L8

### HOW TO STOP ALL PRODUCTION TRAFFIC TO CANARY

Run the following command to drain all traffic from the canary fleet **in production**:

```
/chatops run canary --drain --production

```

This will mark the canary backends as unhealthy which will then result in all canary
traffic being diverted to the non-canary backends.

### Canary ChatOps

The canary chatops command is the primary way to control canary traffic, it has
the following options:

```
Controls canary traffic

Usage: canary [OPTIONS]

Options:

  -h, --help    Shows this help message
  --production  Control production canary traffic instead of staging
  --ready       Set canary to enable connections
  --drain       Set canary to drain connections
  --maint       Set canary to be disabled
  --backend     Filter by a specific backend
```


**IMPORTANT**: Before disabling canary, it is better to drain connections first so that
existing connections can finish:

```
/chatops run canary --drain

```

If it is necessary to _immediately_ stop traffic to canary, use the `maint` command:

```
/chatops run canary --maint

```

* When setting `--{ready,drain,maint}`, the status will be displayed to see the
  result of the change
* Specifying a backend can limit canary to a subset of traffic if desired.
* Don't forget to use `--production` to target production instead of staging, if
  needed

The default canary option is to display the current connection status:

```
/chatops run canary --production

canary_api          : conn:0 UP:32
canary_https_git    : conn:18 UP:32
canary_registry     : conn:5 UP:4
canary_web          : conn:9 UP:32

*UP*: web-cny-01-sv-gprd, web-cny-02-sv-gprd, git-cny-01-sv-gprd, git-cny-02-sv-gprd, api-cny-01-sv-gprd, api-cny-02-sv-gprd, registry-cny-01-sv-gprd, registry-cny-02-sv-gprd
```

The above status shows the number of connections for each backend and the number
of servers reporting `UP`.  Note that there many haproxy servers so the number
of servers reporting for each backend is multiplied by the number of lbs.
