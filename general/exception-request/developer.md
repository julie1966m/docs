# Exception Request as a Developer

## Before opening an exception request

Before opening an exception request, check the [general guidelines](./process.md#after-the-7th)

### Ask yourself the following questions before submitting an exception request:

1. If the change is not included in the release, will it block or break GitLab?
1. Can it wait until the next release cycle?
  Keep in mind that the Release Manager's team will tag the first release candidate on the 1st, chances are the code won't have to wait an entire month to be in production.
1. Do you know the risks and how to mitigate them?
  Do not assume there aren't risks. Every code change has some risk.

## Opening an exception request

1. Go to [release tasks issue tracker](https://gitlab.com/gitlab-org/release/tasks/issues/) and [create an issue using the Exception-request issue template](https://gitlab.com/gitlab-org/release/tasks/issues/new?issuable_template=Exception-request).

    Do not set the relevant Pick into X.Y label before request an exception; this should be done after the exception is approved.

1. Assign the issue to the [Release Managers](https://about.gitlab.com/release-managers/) handling the intended release.

## Filling in the exception request template

### `Why it needs to be picked` section

Although the code changes, scope and context may be obvious to you, keep in mind that the Release Manager may have never heard of them.

When filling this section make sure you add:
1. A detailed explanation of the code changes and their impact. Bear in mind to explain them in a way everyone can understand them, even if that person does not have code experience.
1. A brief explanation on why the deadline was missed
1. Which issue originated the Merge Request - it's enough if it's linked in the MR.
1. Why does it need to be in this release and can't wait a couple of weeks?

### `Potential negative impact of picking` section

No matter how small a change may be, there will always be a risk. Do not assume your change is risk-free.

Here are a few examples of small changes with low risk that broke something:
  1. Transforming a link into a button in the pipelines table caused this regression: The user wasn't able to use the mini-pipeline graph in the MR widget: https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/18976

  1. Changing a z-index in https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/14768 broke the tests in the ce to ee merge blocking a few things for a while: https://gitlab.com/gitlab-org/gitlab-ce/issues/36160#note_43248656

  1. This tiny change https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/18971/diffs caused this regression https://gitlab.com/gitlab-org/gitlab-ce/issues/46754

When filling this section, make sure you add:
1. A detailed explanation of any possible risk
1. **A detailed plan on how to recover from that risk:**
  In a scenario where your change gets to production and breaks something, how do we fix your change without rolling back your colleagues changes too?

  > This is the most important piece of information in this process.

### Examples of well explained exception requests:

If you are still unsure on how to fill the sections above, check these examples of detailed exception requests and the answers to the release manager questions:

- https://gitlab.com/gitlab-org/release/tasks/issues/272
- https://gitlab.com/gitlab-org/release/tasks/issues/218

## After filling the exception requests

Most likely, the Release Manager assigned to the exception request will have questions.

If you find yourself in a situation where you are struggling to explain your point of view, try to imagine you are explaining it for someone who is new to GitLab.

## Understanding why your exception request was rejected

The chances that your exception request was rejected are higher than it being accepted. Do not take this rejection personally. 
Here's what picking a new Merge Request means to the release process:

- The release manager will have to tag a new version for your change, which means
  - Opening a preparation branch for both CE and EE, making sure the change applies properly to both of them (no conflicts, green pipelines)
  - Once the preparation branches are green, they need to be merged into the stable branch
  - Tag a new version, and wait for the pipelines to run (they can take up to 80 minutes, and they can also fail with some flaky specs)
  - Deploy the new tag to staging
  - Wait 12h for the QA to take place, if anything is broken, the release manager will have to wait for a fix and start over
  - Deploy the new tag to canary
  - Deploy the new tag to production

When the release manager does not have enough information on how to solve problems caused by your changes in production or it's not certain QA will succeed, the exception request will not be accepted.
